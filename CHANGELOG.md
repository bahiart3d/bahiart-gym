# CHANGELOG
### Release 1.1.0
- Added an option to set the angle of the beam inside the _beamPlayer()_ method of trainer.py
- Fixed and error on the _acceptConnections()_ method at agentcomms.py. It was not following the intended protocol of reading the firts 4 bytes as the length of the message.
- Added a while loop inside comms.py _init()_ method to wait for the RCSSSERVER3D to be running before attempting to connect.
- Added a thread to constantly update any world.py object:
    - Added a method called _update()_ that runs a _staticUpdate()_ once than opens a infinite while loop to run the _dynamicUpdate()_ method.
    - Added a _getUpdateThread()_ so the thread can be manually created and returned but not started.
    - Added a _autoUpdate_ parameter to World.py init method so the update thread is created and started during the world object instantiation.
- Added an option to reuse both the proxy and agentComms ports so that if a training environment is suddenly stopped, the connection can be restarted even if the ports are in a TIME_WAIT status.
- Added identifying strings, such as "[COMMS]" or "[WORLD]", to the beginning of every print statement
- Removed an unecessary socket creation in trainer.py init method.
- Updated the ball current position variable name from ballFinalPos to ballCurrentPos to better represent its purpose.
- Created a method in world.py to update the number of fouls commited by every player.
- Fixed ball beaming height to its radius.
- Merged the start.py and terminate.py into a Manager class to start and kill trainings.
- Updated trainer strings.
- Updated ball beam string on trainer.py to include velocity.