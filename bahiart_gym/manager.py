"""
        Copyright (C) 2023  Salvador, Bahia
        Gabriel Mascarenhas, Marco A. C. Simões, Rafael Fonseca

        This file is part of BahiaRT GYM.

        BahiaRT GYM is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        BahiaRT GYM is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os, signal, subprocess, time

class Manager(object):

    def __init__(self, config_file: str = "config.ini", sleep_between_processes: int = 0) -> None:
        self.proxyPort = 3800
        self.serverPort = 3100
        self.monitorPort = 3200
        self.config_file = config_file
        self.sleep = sleep_between_processes

    def startEnv(self):
        
        #Reading config.ini in current working directory
        with open(self.config_file,"r") as cfgfile:
            options = cfgfile.readlines()
        
        for opt in options:
            optsplit = opt.split("=")
            if(optsplit[0]=='TRAINING_COMMAND'):
                trainingCommand=optsplit[1].split("\n")[0].split(" ")
            elif(optsplit[0]=="TEAM_COMMAND"):
                teamCommand=optsplit[1].split("\n")[0]
            elif(optsplit[0]=="TEAM_FOLDER"):
                self.teamFolder=optsplit[1].split("\n")[0]
            elif(optsplit[0]=="PROXY_PORT"):
                self.proxyPort=int(optsplit[1].split("\n")[0])
            elif(optsplit[0]=="SERVER_PORT"):
                self.serverPort=int(optsplit[1].split("\n")[0])
            elif(optsplit[0]=="MONITOR_PORT"):
                self.monitorPort=int(optsplit[1].split("\n")[0])
        
        trainingCommand.append(str(self.proxyPort))
        trainingCommand.append(str(self.serverPort))
        trainingCommand.append(str(self.monitorPort))
        
        
        serverCommand = "rcssserver3d"
        
        serverProcess = subprocess.Popen(serverCommand)
        
        time.sleep(self.sleep)
        
        trainingProcess = subprocess.Popen(trainingCommand)
        
        time.sleep(self.sleep)
        
        proxyAvailable=False
        
        while not proxyAvailable:
            try:
                output=subprocess.check_output("lsof -i:"+str(self.proxyPort), shell=True)
                proxyAvailable=True
            except subprocess.CalledProcessError:
                pass

        
        cwd = os.getcwd()
        os.chdir(self.teamFolder)
        teamProcess = subprocess.Popen(teamCommand,cwd=self.teamFolder, shell=True)
        os.chdir(cwd)
        
        return serverProcess.pid, trainingProcess.pid, teamProcess.pid


    def killEnv(self, serverPID=None, envPID=None, teamPID=None) -> None:
        
        if(serverPID is not None):
            print("[TERMINATE] Killing server ...\n")
            os.kill(serverPID, signal.SIGKILL)
        if(envPID is not None):
            print("[TERMINATE] Killing environment ...\n")
            os.kill(envPID, signal.SIGTERM)
        if(teamPID is not None):
            print("[TERMINATE] Killing team ...\n")
            os.kill(teamPID, signal.SIGTERM)

    def forceKillTeam(self, team_dir: str = ""):
        teamCommand = "./kill.sh"
        
        if team_dir == "":
            team_dir = self.teamFolder
        
        cwd = os.getcwd()
        os.chdir(team_dir)
        teamKillProcess = subprocess.Popen(teamCommand,cwd=team_dir, shell=True)
        os.chdir(cwd)