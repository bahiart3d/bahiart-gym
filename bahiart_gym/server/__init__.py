"""
        Copyright (C) 2022  Salvador, Bahia
        Gabriel Mascarenhas, Marco A. C. Simões, Rafael Fonseca

        This file is part of BahiaRT GYM.

        BahiaRT GYM is free software: you can redistribute it and/or modify
        it under the terms of the GNU Affero General Public License as
        published by the Free Software Foundation, either version 3 of the
        License, or (at your option) any later version.

        BahiaRT GYM is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU Affero General Public License for more details.

        You should have received a copy of the GNU Affero General Public License
        along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
from bahiart_gym.server.agentParser import AgentParser
from bahiart_gym.server.agentProxy import AgentProxy
from bahiart_gym.server.ball import Ball
from bahiart_gym.server.comms import Comms
from bahiart_gym.server.parsr import Parser
from bahiart_gym.server.player import Player
from bahiart_gym.server.proxy import Proxy
from bahiart_gym.server.serverParser import ServerParser
from bahiart_gym.server.singleton import Singleton
from bahiart_gym.server.trainer import Trainer
from bahiart_gym.server.world import World